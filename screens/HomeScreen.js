import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
              <ScrollView>
              <Text style={styles.title}>Home Screen</Text>
              <Button
              color="pink"
                title="Details"
                onPress={() => this.props.navigation.navigate('Details')}
              />
              <Button style={styles.space}
              color="pink"
                title="Activity Indicator"
                onPress={() => this.props.navigation.navigate('ActivityIndicator')}
              />
              <Button style={styles.space}
                title="Drawer Layout"
                color="pink"
                onPress={() => this.props.navigation.navigate('DrawerLayout')}
              />
              <Button style={styles.space}
                title="Image Screen"
                color="pink"
                onPress={() => this.props.navigation.navigate('ImageScreen')}
              />
              <Button style={styles.space}
                title="Keyboard Avoid Screen"
                color="pink"
                onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
              />
              <Button style={styles.space}
                title="List View"
                color="pink"
                onPress={() => this.props.navigation.navigate('ListView')}
              />
              <Button style={styles.space}
                title="Modal"
                color="pink"
                onPress={() => this.props.navigation.navigate('ModalScreen')}
              />
              <Button style={styles.space}
                title="Picker Screen"
                color="pink"
                onPress={() => this.props.navigation.navigate('PickerScreen')}
              />
              <Button style={styles.space}
                  title="Progress Bar"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('ProgressBar')}
              />
              <Button style={styles.space}
                  title="Refresh Control"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('RefreshControl')}
              />
              <Button style={styles.space}
                  title="Scroll View"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('ScrollView')}
              />
              <Button style={styles.space}
                  title="Section List"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('SectionList')}
              />
              <Button style={styles.space}
                  title="Slider"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('Slider')}
              />
              <Button style={styles.space}
                  title="Status Bar"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('StatusBar')}
              />
              <Button style={styles.space}
                  title="Switch Screen"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('Switch')}
              />
              <Button style={styles.space}
                  title="Text Input"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('TextInput')}
              />
              <Button style={styles.space}
                  title="Text Screen"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('Text')}
              />
              <Button style={styles.space}
                  title="Touchable HighLight"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('TouchableHighlight')}
              />
              <Button style={styles.space}
                  title="Touchable Native Feedback"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
              />
              <Button style={styles.space}
                  title="Touchable Opacity"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('TouchableOpacity')}
              />
              <Button style={styles.space}
                  title="View Pager Android"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('ViewPager')}
              />
              <Button style={styles.space}
                  title="View Screen"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('View')}
              />
              <Button style={styles.space}
                  title="Web View"
                  color="pink"
                  onPress={() => this.props.navigation.navigate('WebView')}
              />
              </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 25,
      margin: 25,
      textAlign: 'center',
    },
    space: {
      margin: 25,
    }
  });

export default HomeScreen;
